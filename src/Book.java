public class Book<String> {
    private String title;
    private String[] creativePseudonym;
    private int year;
    private int pages;

    public String getTitle() {
        return title;
    }

    public String[] getCreativePseudonym() {
        return creativePseudonym;
    }

    public int getYear() {
        return year;
    }

    public int getPages() {
        return pages;
    }

    public Book(String title,  int year, int pages, String...creativePseudonym) {
        this.title = title;
        this.creativePseudonym = creativePseudonym;
        this.year = year;
        this.pages = pages;
    }

}
