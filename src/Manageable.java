
public interface Manageable {
    public <T extends Manageable, List> void addToDirectories(List<T> catalogs);

    public  <T extends Manageable> void removeFromDirectories(List<T> catalogs);

    public void giveBooksToUsers();

    public <T> void booksWereGiven(List<T> books);

    public <String,Author.FcsAuthor > String identifyAuthorByPseudonym(Map<String, Author.FcsAuthor>);
}

